import {Component, Input, Output, EventEmitter} from "@angular/core";

@Component({
  selector: "mwl-media-item",
  templateUrl: "app/media-item.component.html",
  styleUrls: ["app/media-item.component.css"]
})
export class MediaItemComponent {
  /**
   * create a component prop mediaItem to expose for use in other places
   * also can use alias @Input("mediaItemToWatch")
   */
  @Input() mediaItem;
  /**
   * emitter exposes a doDelete event for further subscription
   */
  @Output() doDelete = new EventEmitter();
  @Output() toggleWatch = new EventEmitter();
  @Output() toggleLike = new EventEmitter();
  onDelete() {
    /**
     * send back mediaItem obj
     */
    this.doDelete.emit(this.mediaItem);
  }
  onWatch() {
    this.toggleWatch.emit(this.mediaItem);
  }
  onLike() {
    this.toggleLike.emit(this.mediaItem);
  }
}