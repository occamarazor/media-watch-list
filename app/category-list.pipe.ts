import {Pipe} from "@angular/core";

@Pipe({
  name: "categoryList",
  pure: true
})
export class CategoryListPipe {
  /**
   * calls a method transform on a pipe class
   */
  transform(mediaItems) {
    let categories = [];
    mediaItems.forEach(mediaItem => {
      if (categories.indexOf(mediaItem.category) === -1) {
        categories.push(mediaItem.category);
      }
    });
    return categories.join(', ');
  }
}
