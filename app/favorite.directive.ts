import {Directive, HostBinding, HostListener, Input} from "@angular/core";
import {MediaItemService} from "./media-item.service";

@Directive({
  /**
   * find a match on an element attribute
   */
  selector: "[mwlFavorite]"
})
export class FavoriteDirective {
  constructor(private mediaItemService: MediaItemService) {}
  
  /**
   * apply a class to a host elem
   * refers to a native DOM prop class
   */
  @HostBinding("class.is-favorite") isFavorite;
  @HostBinding("class.is-favorite-hovering") hovering;
  @HostListener("mouseenter") onMouseEnter () {
    this.hovering = true;
  };
  @HostListener("mouseleave") onMousleLeave () {
    this.hovering = false;
  };
  /**
   * setter called when a prop with same name
   * is set to a value from the instance of the class
   */
  @Input() set mwlFavorite(val) {
    this.isFavorite = val;
  };
}