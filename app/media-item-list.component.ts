import { Component } from "@angular/core";
import {ActivatedRoute} from "@angular/router";

import {MediaItemService} from "./media-item.service";

@Component({
  selector: "mwl-media-item-list",
  templateUrl: "app/media-item-list.component.html",
  styleUrls: ["app/media-item-list.component.css"]
})
export class MediaItemListComponent {
  medium = "";
  mediaItems = [];
  paramsSubscription;
  constructor(
    private mediaItemService: MediaItemService,
    private activatedRoute: ActivatedRoute) {}
  
  ngOnInit() {
    this.paramsSubscription = this.activatedRoute.params
      .subscribe(params => {
        let medium = params["medium"];
        if (medium.toLowerCase() === "all") {
          medium = "";
        }
        this.getMediaItems(medium);
      });
  }
  ngOnDestroy() {
    this.paramsSubscription.unsubscribe();
  }
  
  getMediaItems (medium) {
    this.medium = medium;
    this.mediaItemService.getItem(medium)
      .subscribe(mediaItems => {
        this.mediaItems = mediaItems;
      });
  }
  onMediaItemChange(mediaItem, prop) {
    this.mediaItemService.setItem(mediaItem, prop)
      .subscribe(() => {this.getMediaItems(this.medium)});
  }
  onMediaItemDelete(mediaItem) {
    this.mediaItemService.delItem(mediaItem)
      .subscribe(() => {this.getMediaItems(this.medium)});
  };
}
