import {Injectable} from "@angular/core";
import {Http, URLSearchParams} from "@angular/http";
import "rxjs/add/operator/map";

/**
 * make custom service injectable
 */
@Injectable()
export class MediaItemService {
  constructor(private http: Http) {}
  
  getItem(medium) {
    let searchParams = new URLSearchParams();
    searchParams.append("medium", medium);
    return this.http.get("mediaitems", {search: searchParams})
      .map(response => {
        return response.json().mediaItems;
    });
  }
  addItem(mediaItem) {
    return this.http.post("mediaitems", mediaItem)
      .map(response => {});
  }
  setItem(mediaItem, prop) {
    let searchParams = new URLSearchParams();
    searchParams.append("id", mediaItem.id);
    searchParams.append("property", prop);
    return this.http.post("mediaitems", null, {search: searchParams})
      .map(response => {});
  }
  delItem(mediaItem) {
    return this.http.delete(`mediaitems/${mediaItem.id}`)
      .map(response => {});
  }
}