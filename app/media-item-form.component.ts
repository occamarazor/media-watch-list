import {Component, Inject} from "@angular/core";
import {Validators, FormBuilder} from "@angular/forms";
import {Router} from "@angular/router";

import {MediaItemService} from "./media-item.service";
import {lookupListToken} from "./providers";

@Component({
  selector: "mwl-media-item-form",
  templateUrl: "app/media-item-form.component.html",
  styleUrls: ["app/media-item-form.component.css"]
})
export class MediaItemFormComponent {
  form;
  constructor(
    private formBuilder: FormBuilder,
    private mediaItemService: MediaItemService,
    @Inject(lookupListToken) public lookupLists,
    private router: Router) {}
    
  ngOnInit() {
    this.form = this.formBuilder.group({
      medium: this.formBuilder.control("Movies", Validators.required),
      name: this.formBuilder.control("", Validators.compose([
        Validators.required,
        Validators.pattern("[\\w\\-\\s\\/]+")
      ])),
      category: this.formBuilder.control("", Validators.required),
      year: this.formBuilder.control("", this.yearValidator)
    });
  }
  yearValidator (control) {
    if (control.value === "")
      return null;
    
    let year = parseInt(control.value);
    let minYear = 1900;
    let maxYear = 2050;
    if (year >= minYear && year <= maxYear) {
      return null;
    } else {
      return {"year": {
        min: minYear,
        max: maxYear
      }};
    }
  }
  onSubmit(mediaItem) {
    this.mediaItemService.addItem(mediaItem)
      .subscribe(() => {
        this.router.navigate(["/", mediaItem.medium]);
      });
  }
}
