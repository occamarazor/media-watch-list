import { Request, Response, ResponseOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';

export class MockXHRBackend {
  constructor() {}
  
  createConnection(request: Request) {
    let response = new Observable((responseObserver: Observer<Response>) => {
      let responseOptions;
      switch (request.method) {
        case RequestMethod.Get:
          let medium = request.url.split("=")[1];
          let mediaItems;
          
          if (medium === "undefined")
            medium = "";
          if (medium) {
            mediaItems = this._mediaItems.filter(mediaItem => mediaItem.medium === medium);
          } else {
            mediaItems = this._mediaItems;
          }
          responseOptions = new ResponseOptions({
            body: { mediaItems: JSON.parse(JSON.stringify(mediaItems)) },
            status: 200
          });
          break;
        case RequestMethod.Post:
          if (request.url === "mediaitems") {
            const mediaItem = JSON.parse(request.text().toString());
            mediaItem.id = this._getNewId();
            this._mediaItems.push(mediaItem);
            responseOptions = new ResponseOptions({ status: 201 });
          } else {
            const r = request.url;
            const feq = r.indexOf("=") + 1;
            const amp = r.indexOf("&");
            const id = parseInt(r.slice(feq, amp));
            const prop = r.split("=")[2];
            
            if (prop === "watchedOn") {
              this._changeWatchedOn(id);
            } else {
              this._changeIsFavorite(id);
            }
            responseOptions = new ResponseOptions({ status: 200 });
          }
          break;
        case RequestMethod.Delete:
          const id = parseInt(request.url.split('/')[1]);
          this._deleteMediaItem(id);
          responseOptions = new ResponseOptions({ status: 200 });
      }
      const responseObject = new Response(responseOptions);
      responseObserver.next(responseObject);
      responseObserver.complete();
      return () => { };
    });
    return { response };
  }
  _changeWatchedOn(id) {
    const mediaItem = this._mediaItems.find(mediaItem => mediaItem.id === id);
    if (mediaItem.watchedOn) {
      mediaItem.watchedOn = null;
    } else {
      mediaItem.watchedOn = Date.now();
    }
  }
  _changeIsFavorite(id) {
    const mediaItem = this._mediaItems.find(mediaItem => mediaItem.id === id);
    mediaItem.isFavorite = !mediaItem.isFavorite;
  }
  _deleteMediaItem(id) {
    const mediaItem = this._mediaItems.find(mediaItem => mediaItem.id === id);
    const index = this._mediaItems.indexOf(mediaItem);
    if (index >= 0) {
      this._mediaItems.splice(index, 1);
    }
  }
  _getNewId() {
    if (this._mediaItems.length > 0) {
      return Math.max.apply(Math, this._mediaItems.map(mediaItem => mediaItem.id)) + 1;
    }
    return 1;
  }
  _mediaItems = [
    { id: 1,
      name: "Firebug",
      medium: "Series",
      category: "Science Fiction",
      year: 2010,
      watchedOn: 1294166565384,
      isFavorite: false
    },
    { id: 2,
      name: "The Small Tall",
      medium: "Movies",
      category: "Comedy",
      year: 2015,
      watchedOn: null,
      isFavorite: true
    },
    { id: 3,
      name: "The Redemption",
      medium: "Movies",
      category: "Action",
      year: 2016,
      watchedOn: null,
      isFavorite: false
    },
    { id: 4,
      name: "Hoopers",
      medium: "Series",
      category: "Drama",
      year: null,
      watchedOn: null,
      isFavorite: true
    },
    { id: 5,
      name: "Happy Joe: Cheery Road",
      medium: "Movies",
      category: "Action",
      year: 2015,
      watchedOn: 1457166565384,
      isFavorite: false
    }
  ];
}