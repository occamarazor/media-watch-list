# Angular 4 media watchlist app

Summary: maintain your own media watchlist.  
Create, remove and add to favorite movies and series.  
**View demo at [Mediawatchlist.io](https://mediawatchlist-maxcrank.c9users.io/)**  
*(the app is available only when the server is running)*  
*or follow the instructions to start the app locally.*  

## Instructions
- Install __[node.js](http://nodejs.org/)__
- Install project dependencies  
    `npm install`
- Build and start the app  
    `npm start`

### What's inside  
1. NPM task running
2. System.js packaging
3. Typescript
4. Angular 4
5. Mock XHR Backend